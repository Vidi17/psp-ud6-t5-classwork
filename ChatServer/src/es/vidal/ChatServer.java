package es.vidal;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ChatServer {

    public static final String EXIT = "Salir";

    public static final int PORT = 8888;

    static Map<String, Socket> nickNameSocketMap = new HashMap<>();

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("El servidor de la sala de chat se ha iniciado y está escuchando el puerto: " + PORT);
            while (true) {
                try {
                    Socket socket = serverSocket.accept();
                    System.out.println("Un nuevo usuario está conectado al servidor, la información es: " + socket);
                    new Thread(new ChatServerRunnable(socket)).start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
