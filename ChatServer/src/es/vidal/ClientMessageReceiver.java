package es.vidal;

import java.io.DataInputStream;
import java.io.IOException;

public class ClientMessageReceiver implements Runnable {

    private final DataInputStream dataInputStream;

    private boolean timeToStop = false;

    public ClientMessageReceiver(DataInputStream dataInputStream) {
        this.dataInputStream = dataInputStream;
    }

    @Override
    public void run() {
        while (!timeToStop) {
            try {
                System.out.println(dataInputStream.readUTF());
            } catch (IOException e) {
                if ("Connection reset".equals(e.getMessage())) {
                    System.out.println("¡La conexión al servidor ha sido interrumpida!");
                    break;
                }
                if (!timeToStop) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        timeToStop = true;
    }
}
