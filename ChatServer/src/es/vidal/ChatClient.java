package es.vidal;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try (Socket socket = new Socket("127.0.0.1", 8888);
             DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {

            ClientMessageReceiver messageReceiver = new ClientMessageReceiver(dataInputStream);
            new Thread(messageReceiver).start();
            String input;
            do {
                input = scanner.nextLine();
                write(dataOutputStream, input);
            } while (!ChatServer.EXIT.equalsIgnoreCase(input));
            messageReceiver.stop();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void write(DataOutputStream dataOutputStream, String message) throws IOException {
        dataOutputStream.writeUTF(message);
        dataOutputStream.flush();
    }

}
