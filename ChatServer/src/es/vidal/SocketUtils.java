package es.vidal;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class SocketUtils {

    public static void writeToSocket(Socket socket, String message) throws IOException {
        writeToOutputStream(socket.getOutputStream(), message);
    }

    public static void writeToDataOutputStream(DataOutputStream dataOutputStream, String message) throws IOException {
        dataOutputStream.writeUTF(message);
        dataOutputStream.flush();
    }

    public static void writeToOutputStream(OutputStream outputStream, String message) throws IOException {
        writeToDataOutputStream(new DataOutputStream(outputStream), message);
    }

}
