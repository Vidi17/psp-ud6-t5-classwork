package es.vidal;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;

public class ChatServerRunnable implements Runnable {

    private final Socket socket;

    private final DataOutputStream dataOutputStream;

    private final DataInputStream dataInputStream;

    private String currentUserNickName;

    public ChatServerRunnable(Socket socket) throws IOException {
        this.socket = socket;
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
        this.dataInputStream = new DataInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        try {
            write("¡Bienvenido a la sala de chat!");
            login();
            System.out.println(currentUserNickName + " ha iniciado de sesión con éxito");
            write(currentUserNickName + ", ha iniciado sesión. \n" +
                    "Ingrese [salir] para salir del chat ");

            String input = dataInputStream.readUTF();
            while (!ChatServer.EXIT.equals(input)) {
                System.out.println(currentUserNickName + " escribió: [" + input + "]");
                sendMessage(input);
                input = dataInputStream.readUTF();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ChatServer.nickNameSocketMap.remove(currentUserNickName);
            try {
                dataInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                dataOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void login() throws IOException {
        write("Introduzca su apodo:");
        while (true) {
            String nickName = dataInputStream.readUTF();
            System.out.println("El usuario ha ingresado un apodo: " + nickName);
            synchronized (ChatServerRunnable.class) {
                if (!ChatServer.nickNameSocketMap.containsKey(nickName)) {
                    currentUserNickName = nickName;
                    ChatServer.nickNameSocketMap.put(nickName, socket);
                    break;
                } else {
                    write("El apodo que ingresó ya existe, vuelva a ingresar: ");
                }
            }
        }
    }

    private String formatMessage(String originalMessage) {
        return currentUserNickName + ": " + originalMessage;
    }

    private void sendMessage(String orignalMessage) throws IOException {
        for (Map.Entry<String, Socket> entry : ChatServer.nickNameSocketMap.entrySet()) {
            if (!currentUserNickName.equals(entry.getKey())) {
                SocketUtils.writeToSocket(entry.getValue(), formatMessage(orignalMessage));
            }
        }
    }

    private void write(String message) throws IOException {
        SocketUtils.writeToDataOutputStream(dataOutputStream, message);
    }
}
