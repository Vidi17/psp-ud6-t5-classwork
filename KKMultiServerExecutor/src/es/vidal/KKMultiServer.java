package es.vidal;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KKMultiServer {

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.err.println("Usage: java KKMultiServer <port number> <number of clients>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);
        boolean listening = true;
        ExecutorService executorServiceClientes = Executors.newFixedThreadPool(Integer.parseInt(args[1]));

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            while (listening) {

                KKMultiServerThread kkMultiServerThread = new KKMultiServerThread(serverSocket.accept());
                executorServiceClientes.execute(kkMultiServerThread);
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
            System.exit(-1);
        }
    }
}
